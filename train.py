import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd

from data import generate_dictionary, get_data
import labels


def gen_model(vocab_size, input_shape) -> keras.Model:
    return keras.Sequential([
        keras.layers.Embedding(vocab_size, 8, input_shape=input_shape),
        keras.layers.GlobalAveragePooling1D(),
        keras.layers.Dropout(.8),
        keras.layers.Dense(256, activation=tf.nn.tanh),
        keras.layers.Dropout(.7),
        keras.layers.Dense(64, activation=tf.nn.tanh),
        keras.layers.Dropout(.5),
        keras.layers.Dense(32, activation=tf.nn.tanh),
        keras.layers.Dropout(.3),
        keras.layers.Dense(16, activation=tf.nn.tanh),
        keras.layers.Dense(16, activation=tf.nn.tanh),
        keras.layers.Dense(16, activation=tf.nn.tanh),
        keras.layers.Dense(16, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(8, activation=tf.nn.tanh),
        keras.layers.Dense(3, activation=tf.nn.softmax),
    ])


if __name__ == '__main__':
    dictionary = generate_dictionary(min_count=4)
    reverse_dictionary = dict([(value, key) for (key, value) in dictionary.items()])
    vocab_size = len(dictionary)
    print("training with vocab_size={}".format(vocab_size))


    def decode(text):
        return ' '.join([reverse_dictionary.get(i, '?') for i in text])


    def encode(text):
        encoded = [dictionary.get(word, dictionary["<UNK>"]) for word in text.split(" ")]
        return encoded


    all_data_df = get_data()
    data_df = all_data_df

    train_data = np.array([encode(row.tweet) for row in data_df.itertuples() if isinstance(row.tweet, str)])
    train_data = keras.preprocessing.sequence.pad_sequences(train_data, 500, value=dictionary["<PAD>"], padding="post")
    train_labels = np.array([[
        getattr(row, labels.Objective),
        getattr(row, labels.Positive),
        getattr(row, labels.Negative),
    ] for row in data_df.itertuples() if isinstance(row.tweet, str)])

    model = gen_model(len(dictionary), input_shape=train_data[0].shape)
    model.compile(
        optimizer=keras.optimizers.Adam(),
        loss=keras.losses.binary_crossentropy,
        metrics=["acc"],
    )
    model.summary()
    history = model.fit(
        train_data, train_labels,
        epochs=10,
        batch_size=256,
        validation_split=.20,
        shuffle=True,
        callbacks=[
            keras.callbacks.ModelCheckpoint("arasent_chk.h5", save_best_only=True),
        ]
    )

    model.save("./arasent.h5")

    history_dict = history.history
    history_dict.keys()

    import matplotlib.pyplot as plt

    acc = history_dict['acc']
    val_acc = history_dict['val_acc']

    epochs = range(1, len(acc) + 1)

    plt.clf()  # clear figure

    plt.plot(epochs, acc, 'bo', label='Training acc')
    plt.plot(epochs, val_acc, 'b', label='Validation acc')
    plt.title('Training and validation accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.grid()
    axes = plt.gca()
    axes.set_ylim([0.6, 1])

    plt.show()
