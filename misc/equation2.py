import tensorflow as tf
from numpy.matlib import rand
from tensorflow import keras
import numpy as np

Dense = keras.layers.Dense

data_size = 50_000


def get_data():
    try:
        ds_data = np.load("./equation2_data.npy")
        ds_labels = np.load("./equation2_labels.npy")

        train_data = ds_data[0]
        validation_data = ds_data[1]
        train_labels = ds_labels[0]
        validation_labels = ds_labels[1]

    except IOError:
        train_data = np.random.random_integers(-50, 50, (data_size, 2))
        train_labels = np.array([[e.sum()] for e in train_data])

        validation_data = np.random.random_integers(-50, 50, (data_size, 2))
        validation_labels = np.array([[e.sum()] for e in validation_data])
        np.save("./equation2_data.npy", [train_data, validation_data])
        np.save("./equation2_labels.npy", [train_labels, validation_labels])

    return (train_data, train_labels), (validation_data, validation_labels)


if __name__ == '__main__':
    (train_data, train_labels), (validation_data, validation_labels) = get_data()

    model = keras.Sequential([
        Dense(4, activation=tf.nn.relu),
        Dense(1, activation=tf.nn.leaky_relu),
    ])

    model.compile(
        optimizer=tf.keras.optimizers.Adam(1e-2),
        loss=tf.losses.mean_squared_error,
        metrics=['acc', 'mean_absolute_error', 'mean_squared_error'],
    )
    history = model.fit(
        train_data,
        train_labels,
        batch_size=250,
        epochs=50,
        validation_data=(validation_data, validation_labels),
    )
    # model.save("./model")
    # array = np.array([[3, 4], [0, 0], [1, 1], [2, 2], [100, 230], [233, 67], [-30, -50]])

    array = np.array([[-30, -50], [-30, -100], [-300, -150]])
    out = model.predict(array)

    print(out)
