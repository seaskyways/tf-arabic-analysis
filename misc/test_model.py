import tensorflow as tf
import numpy as np

if __name__ == '__main__':
    model = tf.keras.models.load_model("./model")
    print(model.predict(np.array([[100, 550]])))
