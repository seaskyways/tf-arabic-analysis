import tensorflow as tf

x, y = tf.constant([[1., 2.]]), tf.constant([[12., 4.]])
z = tf.Variable([[0.0, 0.0]])

yy = tf.add(x, z)
deviation = tf.square(y - yy)

train_step = tf.train.GradientDescentOptimizer(0.01).minimize(deviation)

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

for i in range(50000):
    sess.run(train_step)

if __name__ == '__main__':
    print(sess.run(z))
