import pandas as pd
import numpy as np
import labels


def clean():
    new_data = pd.concat([
        convert_numbered_data(pd.read_csv("./data/datasets/RES.csv")),
        convert_numbered_data(pd.read_csv("./data/datasets/HTL.csv")),
        convert_numbered_data(pd.read_csv("./data/datasets/PROD.csv")),
        convert_numbered_data(pd.read_csv("./data/datasets/ATT.csv")),
    ])

    data = pd.read_csv("./data/Tweets.tsv", sep="\t", header=None, names=["tweet", "tag"])
    data[labels.Objective] = data.apply(axis=1, func=lambda row: row["tag"][:3] == labels.Objective)
    data[labels.Positive] = data.apply(axis=1, func=lambda row: row["tag"][:3] == labels.Positive)
    data[labels.Negative] = data.apply(axis=1, func=lambda row: row["tag"][:3] == labels.Negative)

    reviews = pd.read_csv("./data/Reviews_onehot.csv")

    data = pd.concat([data, new_data, reviews], sort=True, copy=False)

    from old.AraTweet import AraTweet
    ara = AraTweet()

    def clean_row(row):
        return ara.clean_raw_review(row["tweet"])

    data["tweet"] = data.apply(axis=1, func=clean_row)

    del data["tag"]
    data = data[data["tweet"].map(len) > 0]
    return data


def convert_numbered_data(res_data):
    new_data = pd.DataFrame(columns=["tweet", "OBJ", "POS", "NEG"])
    for i, row in res_data.iterrows():
        new_data = new_data.append({"tweet": row["text"], "OBJ": row["polarity"] == 0, "POS": row["polarity"] > 0,
                                    "NEG": row["polarity"] < 0}, ignore_index=True)
    return new_data


def get_data() -> pd.DataFrame:
    data = pd.read_csv("./data/Tweets_onehot.tsv", sep="\t")
    return data


def generate_dictionary(most_used=1_000_000, min_count=0):
    i = -1

    def get_id():
        nonlocal i
        i = i + 1
        return i

    d = {
        "<PAD>": get_id(),
        "<UNK>": get_id(),
        "<UNUSED>": get_id(),
    }

    data = get_data()
    word_count = {}

    for sentence in data["tweet"]:
        if isinstance(sentence, str):
            words = sentence.split(" ")
            for word in words:
                if word not in word_count:
                    word_count[word] = 1
                else:
                    word_count[word] = word_count[word] + 1

    df = pd.DataFrame(data={"word": [w for w in word_count], "count": [c for c in word_count.values()]})
    df.sort_values(by=["count"], inplace=True, ascending=False)
    df.reset_index(drop=True, inplace=True)
    if min_count > 1:
        df = df[df["count"] >= min_count]
    df = df[:most_used].copy()
    df.to_csv("words.csv")

    for i, row in df.iterrows():
        d[row["word"]] = get_id()

    return d


if __name__ == '__main__':
    data = clean()
    data.to_csv("./data/Tweets_onehot.tsv", sep="\t", index=False)
    print(get_data().tail())
    print("dictionary: {}".format(len(generate_dictionary().keys())))
