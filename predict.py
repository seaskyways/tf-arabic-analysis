import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd

from data import generate_dictionary, get_data
import labels
import matplotlib.pyplot as plt


def load_model() -> keras.Model:
    return keras.models.load_model("./arasent.h5")


if __name__ == '__main__':
    dictionary = generate_dictionary()
    reverse_dictionary = dict([(value, key) for (key, value) in dictionary.items()])
    labels = [labels.Objective, labels.Positive, labels.Negative]


    def decode(text):
        return ' '.join([reverse_dictionary.get(i, '?') for i in text])


    def encode(text):
        encoded = [dictionary.get(word, dictionary["<UNK>"]) for word in text.split(" ")]
        return encoded


    model = load_model()
    data = np.array([
        encode("بعد استقالة رئيس المحكمة الدستورية ننتظر استقالة رئيس القضاء السودان"),
        encode("أنا سعيد جدا"),
        encode("سعيد جدا حياة أزهار صباح"),
        encode("أهنئ الدكتور أحمد جمال الدين القيادي بحزب مصر بمناسبة صدور أولى روايته"),
        encode("الصداقة تزرع الحياة أزهارا مي زيادة"),
        encode("كل شى كتبته غلط كل شى حسبته غلط فى الامتحانات"),
        encode("امير عيد هو اللي فعلا يتقال عليه ستريكر صريح كاريوكي السكة شمال"),
    ])
    model.summary()
    data = keras.preprocessing.sequence.pad_sequences(data, 200, value=dictionary["<PAD>"], padding="post")
    prediction: np.ndarray = model.predict(data)
    prediction = prediction * 100
    prediction = prediction.round()
    pred_labels = [labels[row.argmax()] for row in prediction]
    print(pred_labels)
