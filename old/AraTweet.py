# -*- coding: utf-8 -*-
"""
Created on Sun Mar 10 16:27:03 2015
"""

import codecs

import numpy as np
import pandas as pd
import re


class AraTweet:

    # Copied from the PyArabic package.
    @staticmethod
    def arabicrange():
        """return a list of arabic characteres .
        Return a list of characteres between \u060c to \u0652
        @return: list of arabic characteres.
        @rtype: unicode;
        """
        mylist = []
        for i in range(0x0600, 0x00653):
            try:
                mylist.append(chr(i))
            except ValueError:
                pass
        return mylist

    # cleans a single review
    def clean_raw_review(self, b):
        # patterns to remove first
        pat = [
            ("http[s]?://[a-zA-Z0-9_\\-./~\\?=%&]+", ''),  # remove links
            ('www[a-zA-Z0-9_\\-?=%&/.~]+', ''),
            #            '\\n+': ' ',                     # remove newlines
            ('<br />', ' '),  # remove html line breaks
            ('</?[^>]+>', ' '),  # remove html markup
            #            'http': '',
            ('[a-zA-Z]+\\.org', ''),
            ('[a-zA-Z]+\\.com', ''),
            ('://', ''),
            ('&[^;]+;', ' '),
            (':D', ':)'),
            ('[0-9/]+', ''),
            #            '[a-zA-Z.]+': '',
            #            '[^0-9' + ''.join(self.arabicrange()) + \\
            #                u"!.,;:$%&*%'#(){}~`\\[\\]/\\\\\"" + \
            #                '\s^><\-_\u201D\u00AB=\u2026]+': '',          # remove latin characters
            ('\\s+', ' '),  # remove spaces
            ('\\.+', '.'),  # multiple dots
            ('[\u201C\u201D]', '"'),  # “
            ('[\u2665\u2764]', ''),  # heart symbol
            ('[\u00BB\u00AB]', '"'),
            ('\u2013', '-'),  # dash
        ]

        # patterns that disqualify a review
        remove_if_there = [
            '[^0-9' + ''.join(self.arabicrange()) +
            u"!.,;:$%&*%'#(){}~`\\[\\]/\\\\\"" +
            '\\s\\^><\\-_\u201D\u00AB=\u2026+|' +
            '\u0660-\u066D\u201C\u201D' +
            '\ufefb\ufef7\ufef5\ufef9]+',  # non arabic characters
        ]

        # patterns that disqualify if empty after removing
        # remove_if_empty_after = [
        #     ('[0-9a-zA-Z\\-_]', ' '),  # alpha-numeric
        #     ('[0-9' + u".,!;:$%&*%'#(){}~`\\[\\]/\\\\\"" +
        #      '\\s\\^><`\\-=_+]+', ''),  # remove just punctuation
        #     ('\\s+', ' '),  # remove spaces
        # ]

        # remove again
        # patterns to remove
        pat2 = [
            #            '[^0-9' + ''.join(self.arabicrange()) + \
            #                u"!.,;:$%&*%'#(){}~`\[\]/\\\\\"" + \
            #                '\s^><\-_\u201D\u00AB=\u2026]+': '',          # remove latin characters
        ]

        # if empty body, skip
        if b == '':
            return ""

        b = b.replace("#", "")
        b = re.sub("[\\u064B-\\u0659]", "", b)
        b = re.sub('[_,.!?@"\'()*$%^&=+\\-`~:;\\[\\]|/\\\\<>]', " ", b)
        b = re.sub("[.؛؟،]", " ", b)
        b = re.sub("[\\u0660-\\u066D]+", " ", b)


        # do some substitutions
        for k, v in pat:
            b = re.sub(k, v, b)

        # remove if exist
        for k in remove_if_there:
            if re.search(k, b):
                return ""

        # remove if empty after replacing
        # for k, v in remove_if_empty_after:
        #     temp = re.sub(k, v, b)
        #     if temp == u" " or temp == u"":
        #         return ""

        # if empty string, skip
        return b.strip()
